package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path"
)

func usage() {
        callName := os.Args[0]
        fmt.Fprintf(os.Stderr, "usage: %s [options]\n", callName)
        fmt.Fprintf(os.Stderr, "\tManage xorg keymaps.\n\n")
        fmt.Fprintf(os.Stderr, "options:\n")
        fmt.Fprintf(os.Stderr, "  -add keymap\n\t\tAdd keymap by name.\n")
        fmt.Fprintf(os.Stderr, "  -list\n\t\tList keymaps.\n")
        fmt.Fprintf(os.Stderr, "  -remove keymap\n\t\tRemove named keymap.\n")
        fmt.Fprintf(os.Stderr, "  -file filename\n\t\tDefine path to layout json.\n")
        fmt.Fprintf(os.Stderr, "\nWith no options, cycles through keymaps one by one.\n")
        os.Exit(2)
}

func main() {
	flagAdd := flag.String("add", "", "add a new layout")
	flagRemove := flag.String("remove", "", "remove a layout")
	flagList := flag.Bool("list", false, "list layouts")
	flagFile := flag.String("file", "", "set path to layout json")
	log.SetPrefix(fmt.Sprintf("%s: ", os.Args[0]))
	log.SetFlags(0)
	flag.Usage = usage
	flag.Parse()

	if *flagFile == "" {
		*flagFile = path.Join(os.Getenv("HOME"), ".local/share/keys.json")
	}

	keyFile, err := openFile(*flagFile)
	if err != nil {
		log.Fatal("Couldn't find or make layout json file: ", err)
	}
	defer keyFile.Close()

	carousel, err := loadCarousel(keyFile)
	if err != nil {
		log.Fatal("Couldn't read layout json: ", err)
	}

	if *flagList {
		fmt.Println(carousel.Sets)
		return
	}

	if *flagRemove != "" {
		err := carousel.Remove(*flagRemove)
		if err != nil {
			log.Fatal("Couldn't remove: ", err)
		}

		err = carousel.Save(keyFile)
		if err != nil {
			log.Fatal("Couldn't save layout file: ", err)
		}
		return
	}

	if *flagAdd != "" {
		carousel.Add(*flagAdd)
		err := carousel.Save(keyFile)
		if err != nil {
			log.Fatal("Couldn't save layout file: ", err)
		}
		return
	}

	if len(carousel.Sets) < 1 {
		return
	}

	carousel.Index ++
	if carousel.Index >= len(carousel.Sets) {
		carousel.Index = 0
	}

	err = set(carousel.Sets[carousel.Index])
	if err != nil {
		log.Fatal("Couldn't set keymap: ", err)
	}
	err = carousel.Save(keyFile)
	if err != nil {
		log.Fatal("Couldn't save layout file: ", err)
	}
	return
}

// Carousel represents a set of keymaps as well as the current index in use.
type Carousel struct {
	Sets  []string
	Index int
}

// Remove removes a keymap in a Carousel by name.
func (carousel *Carousel) Remove(target string) error {
	for i, set := range carousel.Sets {
		if set == target {
			copy(carousel.Sets[i:], carousel.Sets[i+1:])
			carousel.Sets[len(carousel.Sets)-1] = ""
			carousel.Sets = carousel.Sets[:len(carousel.Sets)-1]
			return nil
		}
	}
	return errors.New("no such keymap")
}

// Add adds a new keymap to a Carousel by name.
func (carousel *Carousel) Add(set string) {
	carousel.Sets = append(carousel.Sets, set)
}

// Save saves a keymap to a file in a json.
func (carousel *Carousel) Save(file *os.File) error {
	byteFile, err := json.MarshalIndent(carousel, "", "\t")
	if err != nil {
		return err
	}
	file.Truncate(0)
	file.Seek(0, 0)
	_, err = file.Write(byteFile)
	return err
}

func loadCarousel(file *os.File) (*Carousel, error) {
	byteFile, err := ioutil.ReadAll(file)
	if err != nil {
		return new(Carousel), err
	}

	carousel := &Carousel{}
	err = json.Unmarshal(byteFile, carousel)
	if err != nil {
		carousel = new(Carousel)
	}
	return carousel, nil
}

func openFile(filename string) (*os.File, error) {
	return os.OpenFile(filename, os.O_CREATE|os.O_RDWR, 0644)
}

func set(name string) error {
	cmd := exec.Command("setxkbmap", name)
	_, err := cmd.Output()
	return err
}
