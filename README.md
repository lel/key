# key

A little cli tool to manage xorg keymaps.

## Install

```bash
go get git.envs.net/lel/key
```

## Usage

To cycle to the next keymap in the list: `key`.

To add a keymap: `key -add us`

To remove a keymap: `key -remove us`

To list all keymaps being tracked: `key -list`

To define the path to the json file: `key -file ~/.layouts.json` (defaults to `$HOME/.local/share/keys.json`).


## Real world usage

Map `key` to some keybinding, then add a bunch of keymaps to it.

Then that keymap can cycle through keymaps. Note that if your keymap changes the key that you bound it to, then you may be stuck, if you get what I mean by that. Like if you bind it to `ctrl-r`, and then you change to a japanese layout or something, you won't have an `r` and you won't be able to get back.

My wm, [spectrwm](https://github.com/conformal/spectrwm), doesn't do that, and lets you keep using the same keybind even if it doesn't exist in the current keymap. I don't know how or why that happens.

I haven't looked to see if `sxhkd` works for that, but w/e idc about you this is for me lmao.
